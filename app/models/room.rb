class Room < ActiveRecord::Base

	validates :name, case_sensitive: false, uniqueness: true, presence: true

	validates :address, presence: true, uniqueness: true, case_sensitive: false

	validates :capacity, presence: true, numericality: { greater_than: 0}

	has_many :guests


end
