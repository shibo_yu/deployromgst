class Guest < ActiveRecord::Base
	validates :name, case_sensitive: false, uniqueness: true, presence: true

	validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create },
										presence: true, uniqueness: true, case_sensitive: false
	validates :room_id, presence: true, numericality: { only_integer: true}

	belongs_to :room

	def creathelper
		@room = Room.find_by_id(room_id)
		if @room
			if @room.guests.size >= @room.capacity
				return false
			else
				return true
			end
		else
			if room_id < 0 
				return true
			else
				return false
			end
		end
	end

end
